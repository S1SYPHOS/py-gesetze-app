export default () => ({
    label: 'Text kopieren',

    copy() {
        navigator.clipboard.writeText(this.$refs.text.innerText)
        this.label='Text kopiert!'

        setTimeout(() => {
            this.label = 'Text kopieren';
        }, 3000)
    },
})
