import Alpine from 'alpinejs'

// Import modules
import copy from './components/copy'
import cursor from './components/cursor'
import dropdown from './components/dropdown'
import terminal from './components/terminal'
import textbox from './components/textbox'

// Add data objects
Alpine.data('copy', copy)
Alpine.data('cursor', cursor)
Alpine.data('dropdown', dropdown)
Alpine.data('terminal', terminal)
Alpine.data('textbox', textbox)

// Initialize `alpinejs`
Alpine.start()
