# Rechtslinks as an App
[![License](https://badgen.net/badge/license/GPL/blue)](https://codeberg.org/S1SYPHOS/gesetzesapp/src/branch/main/LICENSE) [![Build](https://ci.codeberg.org/api/badges/S1SYPHOS/gesetzesapp/status.svg)](https://codeberg.org/S1SYPHOS/gesetzesapp/issues)

Eine Python-Webapplikation, mit der Rechtsnormen bequem in Hyperlinks zum jeweiligen Gesetz umgewandelt werden können.


## Über dieses Projekt

Dieses Projekt soll einen (kleinen) Beitrag dazu leisten, Autor:innen die Arbeit mit juristischen Texten im Vorfeld einer Veröffentlichung im Internet zu erleichtern:

> **Beispiel:** Referendarin Petra Müller soll im Rahmen ihrer Verwaltungsstation eine neue Verwaltungsvorschrift erklären und alle verwendeten Bundesnormen auf "Gesetze im Internet" verlinken - das CMS ist leider veraltet, weshalb eine *automatische Umwandlung nicht möglich* ist. Uff, erstmal einen Kaffee ..

**Das Problem:** Bereits vor dem Ersten Staatsexamen gehört es zum Tagesgeschäft aller Jurist:innen, sich mittels *copy&paste* durch das Internet zu bewegen - eine müßige und undankbare Tätigkeit, die jede:r am liebsten vermeiden will.

**Die Lösung:** So entstand die Idee, eine Anwendung bereitzustellen, die vielseitig verwendbar und einfach zu bedienen ist, um auch größere Texte schnell für die Veröffentlichung in einem Onlinemedium aufzubereiten.


## Über die Technik

Die Anwendung ist in der Programmiersprache [Python](https://de.wikipedia.org/wiki/Python_(Programmiersprache)) geschrieben und basiert auf dem Webframework [Flask](https://de.wikipedia.org/wiki/Flask). Darauf aufbauend kommen das CSS-Framework [Windi CSS](https://windicss.org) sowie das JavaScript-Framework [Alpine.js](https://alpinejs.dev) zum Einsatz.

In einem ersten Schritt extrahiert die Anwendung alle Rechtsnormen aus dem zur Verfügung gestellten Text, ohne diese jedoch zu speichern - hierbei bedient sie sich der Programmbibliothek [`py-gesetze`](https://codeberg.org/S1SYPHOS/py-gesetze).

An dieser Stelle haben Nutzer:innen die Möglichkeit, Links in den Formaten HTML oder Markdown zu erzeugen und **individuell anzupassen**, etwa durch Auswahl eines geeigneten Titelattributs - andernfalls werden sinnvolle Voreinstellungen verwendet.

Anschließend wird **jede gefundene Rechtsnorm durch einen Hyperlink ersetzt** und schließlich der angepasste Text zurückgegeben.

> **Hinweis:** Der gesamte Prozess findet *in-memory* statt, sodass auf dem Server keinerlei (personenbezogene) Daten gespeichert werden.


## Setup

Aktuell läuft die App auf einer [uberspace](https://uberspace.de)-Instanz: Dort wird der WSGI-Server [`gunicorn`](https://gunicorn.org) mittels `supervisor` in der nachfolgenden Konfiguration gestartet:

```ini
[program:flask]
directory=%(ENV_HOME)s/gesetzesapp
command=%(ENV_HOME)s/gesetzesapp/ENV/bin/gunicorn -c wsgi.py
autostart=true
autorestart=true
```

Happy coding!
