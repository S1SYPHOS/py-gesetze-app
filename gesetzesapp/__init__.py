import os

from logging.handlers import RotatingFileHandler

from flask import Flask
from flask_fingerprint import Fingerprint
from flask_minify import minify
from werkzeug.exceptions import HTTPException

from .config import config
from .errors import handle_error
from .filters import filters
from .globals import current_version
from .views import *


def create_app():
    """
    Createss new app instance
    """

    # Create app
    app = Flask(__name__, static_folder='assets')

    # Load configuration
    app.config.from_object(config[app.env])

    # Activate logging
    # (1) Define directory
    log_dir = os.path.join(app.instance_path, 'logs')

    # (2) Make sure it exists
    try:
        os.makedirs(log_dir)

    except OSError:
        pass

    # (3) Initialize log handler
    log_handler = RotatingFileHandler(
        filename=os.path.join(log_dir, app.config['LOG_FILE']),
        maxBytes=app.config['LOG_MAX_BYTES'],
        backupCount=app.config['LOG_BACKUP_COUNT']
    )

    # (2) Add log handler & set loglevel
    app.logger.addHandler(log_handler)
    app.logger.setLevel(app.config['LOG_LEVEL'])

    # Add template filters
    app.jinja_env.filters.update(filters)

    # Add global functions
    app.jinja_env.globals.update(current_version=current_version)

    # Add error handler
    app.register_error_handler(HTTPException, handle_error)

    # Register blueprints
    # (1) App
    app.register_blueprint(api)

    # (2) Pages & directories
    app.register_blueprint(about, url_prefix='/ueber-das-projekt')
    app.register_blueprint(legal, url_prefix='/rechtliche-angaben')
    app.register_blueprint(logs, url_prefix='/logs')

    # Enable asset fingerprinting
    Fingerprint(app)

    # Minify HTML output
    if not app.debug:
        minify(app=app, html=True, js=True)

    return app
