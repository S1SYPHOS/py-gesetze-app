from .api import api
from .about import about
from .legal import legal
from .logs import logs

__all__ = [
    'api',
    'about',
    'legal',
    'logs',
]
