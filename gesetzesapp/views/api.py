from flask import current_app as app
from flask import Blueprint, request, render_template, abort
from markupsafe import escape

from gesetze import Gesetz

from ..utils import by_law, human_size


# Initialize blueprint
api = Blueprint('api', __name__)


@api.route('/', methods = ['GET', 'POST'])
def index():
    # Redirect 'GET' requests ..
    if request.method == 'GET':
        # .. to `form` template
        return render_template('api/form.html')

    # Check field inputs
    if not request.form['text']:
        abort(400)

    # Fetch form parameters
    # (1) Output mode
    mode = escape(request.form['lang'])

    # (2) Title attribute
    title = escape(request.form['title'])

    # (3) Target attribute
    target = escape(request.form['target'])

    try:
        # Create linking instance
        obj = Gesetz()

        # Fetch input text
        text = escape(request.form['text'])

        # Check whether norms are present
        norms = obj.extract(text)

        # If no legal norm detected ..
        if not norms:
            # .. report back
            return render_template('api/form.html', message='Der eingegebene Text enthält (soweit ersichtlich) keine Rechtsnormen!')

        # Determine callback function
        cb = obj.markdownify if mode == 'Markdown' else obj.linkify

        # Create data array
        data = {}

        # If selected ..
        if title in ['light', 'normal', 'full']:
            # (1) .. apply 'title' option
            obj.title = title

            # (2) .. remember it
            data['title'] = title

        # If not selected ..
        if target != 'blank':
            # .. deactivate 'target' attribute
            obj.attributes = {}

        # Store information
        # (1) Processed text
        data['text'] = obj.gesetzify(text, cb)

        # (2) Text length
        data['count'] = len(data['text'])

        # (3) Output mode
        data['mode'] = mode

        # Extract legal norms
        # (1) Group by law
        # (2) Sort them
        data['norms'] = dict(sorted(by_law(norms).items()))

    # If something goes south ..
    except Exception as error:
        # .. report back
        app.logger.exception(error)

        # .. exit with 'FileNotFound' error
        abort(404)

    # Render template
    return render_template('api/data.html', data=data)


@api.errorhandler(400)
def bad_request(error):
    return render_template('api/form.html', message='Kein Text eingegeben!', error=True), 400


@api.errorhandler(404)
def file_not_found(error):
    return render_template('api/form.html', message='Ein Fehler ist aufgetreten, bitte versuchen Sie es später noch einmal!', error=True), 404
