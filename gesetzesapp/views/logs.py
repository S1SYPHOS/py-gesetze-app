from flask import Blueprint, abort


# Initialize blueprint
logs = Blueprint('logs', __name__)


@logs.route('/')
def index():
    abort(403)
